<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.

ini_set('display_startup_errors', 1);
ini_set('display_startup_errors', 1);
// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************
$errors = false;

if (empty($_POST['fio'])){
    $errors = true;
    print("Заполните имя и фамилию.<br/>");
}
if (empty($_POST['email'])){
    $errors = true;
    print("Заполните email.<br/>");
}
if (empty($_POST["floor"])){
    $errors = true;
    print("Заполните пол.<br/>");
}
if (empty($_POST['limbs'])){
    $errors = true;
    print("Заполните кол-во конечностей.<br/>");
}
if (empty($_POST['birthdate'])){
  $errors = true;
  print("Заполните дату рождения.<br/>");
}
if (empty($_POST['biography'])){
  $errors = true;
  print("Заполните биографию.<br/>");
}
if (empty($_POST['agree'])){
  $errors = true;
  print("Заполните согласие.<br/>");
}


if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.
//print_r($_POST);
$user = 'u20946';
$pass = '9860142';
try {
$db = new PDO('mysql:host=localhost;dbname=u20946', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
// Подготовленный запрос. Не именованные метки.
$name = $_POST["fio"];
$mail = $_POST["email"];
$birthdate = $_POST["birthdate"];
$sex = $_POST["floor"];
$limbs = $_POST["limbs"];
$biography = $_POST["biography"];
  $sql = "INSERT into application set name = ?, mail = ?, birthdate = ?, sex = ?, limbs = ?, biography = ?";
  $stmt = $db->prepare($sql);
  $stmt -> execute([$name, $mail, $birthdate, $sex, $limbs, $biography]);
  $db -> exec($sql);
  $last_id = $db->lastInsertId();

  $sql = "INSERT into app_sup set id = ?, sup_id =?";
  $stmt = $db->prepare($sql);
  foreach($_POST["superskills"] as $val){
  $stmt->execute([$last_id, $val]);
  }
}//
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}

//  stmt - это "дескриптор состояния".
 
//  Именованные метки.
//$stmt = $db->prepare("INSERT INTO test (label,color) VALUES (:label,:color)");
//$stmt -> execute(array('label'=>'perfect', 'color'=>'green'));
 
//Еще вариант
/*$stmt = $db->prepare("INSERT INTO users (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
$stmt->bindParam(':firstname', $firstname);
$stmt->bindParam(':lastname', $lastname);
$stmt->bindParam(':email', $email);
$firstname = "John";
$lastname = "Smith";
$email = "john@test.com";
$stmt->execute();
*/

// Делаем перенаправление.
// Если запись не сохраняется, но ошибок не видно, то можно закомментировать эту строку чтобы увидеть ошибку.
// Если ошибок при этом не видно, то необходимо настроить параметр display_errors для PHP.
header('Location: ?save=1');
